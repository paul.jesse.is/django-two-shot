from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm, CategoryForm, AccountForm
from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required
def receipt_list(request):
    list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": list
    }
    return render(request, "receipts/receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expense_categories": list
    }
    return render(request, "receipts/expense_categories.html", context)


@login_required
def account_list(request):
    list = Account.objects.filter(owner=request.user)
    context = {
        "accounts": list
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("expense_category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
